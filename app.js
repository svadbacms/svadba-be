const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const authMiddleware = require('./routes/middleware');
const users = require('./routes/users');
const tasks = require('./routes/tasks');
const task = require('./routes/task');
const comment = require('./routes/comment');
const mongoService = require('./services/mongoService');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/users', users);

// route middleware to verify a token
app.use(authMiddleware);
app.use('/tasks', tasks);
app.use('/task', task);
app.use('/comment', comment);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

mongoService.connectOnce(function(db) {
  console.log('DB connected to: ', db.s.databaseName);
});

module.exports = app;

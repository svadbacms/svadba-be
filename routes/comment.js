const express = require('express');
const router = express.Router();
const ObjectID = require('mongodb').ObjectID;
const commentService = require('../services/commentService');

router.options('/', function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, POST, DELETE, GET');
  res.header('Access-Control-Allow-Headers', 'content-type, x-access-token');
  res.send(null);
});

// create comment
router.put('/', function(req, res, next) {
  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');

  const document = req.body;
  document._id = new ObjectID();
  document.createdAt = new Date().getTime();
  document.taskId = new ObjectID(document.taskId);

  commentService.createComment(document)
    .then(r => {
      res.status(201);
      res.send(document);
    })
    .catch(e => {
      res.status(500);
      res.send(e);
    });
});

// update comment
router.post('/', function(req, res, next) {
  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');

  const document = req.body;
  document._id = new ObjectID(document._id);

  commentService.updateComment(document)
    .then(r => {
      res.send(document);
    })
    .catch(e => {
      res.status(500);
      res.send(e);
    });
});

// delete comment
router.delete('/', function(req, res, next) {
  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');

  const commentId = new ObjectID(req.body._id);

  commentService.deleteTask(commentId)
    .then(r => {
      res.send({});
    })
    .catch(e => {
      res.status(500);
      res.send(e);
    });
});

module.exports = router;

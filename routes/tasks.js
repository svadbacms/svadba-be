const express = require('express');
const router = express.Router();

const taskService = require('../services/taskService');
const commentService = require('../services/commentService');

router.options('*', function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, POST, DELETE, GET');
  res.header('Access-Control-Allow-Headers', 'content-type, x-access-token');
  res.send();
});

router.get('/', function(req, res, next) {
  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');

  taskService.getTasks()
    .then(tasks => {
      commentService.getComments()
        .then(comments =>res.send({comments, tasks}))
        .catch(e => {
          res.status(500);
          res.send(e);
        });
    })
    .catch(e => {
      res.status(500);
      res.send(e);
    });
});

module.exports = router;

const express = require('express');
const router = express.Router();
const userService = require('../services/userService');
const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const config = require('../config');

router.options('*', function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, POST, DELETE, GET');
  res.header('Access-Control-Allow-Headers', 'content-type,  x-access-token');
  res.send();
});

router.post('/authenticate', function(req, res, next) {
  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');

  userService.getUser(req.body.name)
    .then(data => {
      if (data && req.body.password === data.password) {
        const user = {
          name: data.name,
          role: data.role
        };
        const token = jwt.sign(user, config.auth.secret, {
          expiresIn: 86400 // expires in 24 hours
        });

        // return the information including token as JSON
        res.json({
          success: true,
          message: 'Enjoy your token!',
          token: token
        });
      } else {
        res.status(401);
        res.json({ success: false, message: 'Authentication failed. User not found.' });
      }
    })
    .catch(e => {
      res.status(500);
      res.send(e);
    });
});

router.post('/toggleNotifications', function (req, res, next) {
  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');
  const token = jwt.decode(req.header('x-access-token'));
  const sendNotifications = req.body.sendNotifications;
  userService.toggleNotifications(token.name, sendNotifications)
    .then(r => res.send(req.body))
    .catch(e => {
      res.status(500);
      res.send(e);
    });
});


router.get('/info', function (req, res, next) {
  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');
  const token = jwt.decode(req.header('x-access-token'));
  userService.getUser(token.name)
    .then(r => res.send({name: r.name, role: r.role, sendNotifications: r.sendNotifications}))
    .catch(e => {
      res.status(500);
      res.send(e);
    });
});

module.exports = router;

const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const config = require('../config');

module.exports = function(req, res, next) {
  // remove auth in OPTIONS requests
  if (req.method === 'OPTIONS') {
    next();
    return;
  }
  // check header or url parameters or post parameters for token
  const token = req.body.token || req.query.token || req.headers['x-access-token'];

  // decode token
  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, config.auth.secret, function(err, decoded) {
      if (err) {
        res.header('Content-Type', 'application/json');
        res.header('Access-Control-Allow-Origin', '*');
        res.status(401);
        return res.send({ success: false, message: 'Failed to authenticate token.' });
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;
        next();
      }
    });
  } else {
    // if there is no token
    // return an error
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    });
  }
};

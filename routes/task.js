const express = require('express');
const router = express.Router();
const ObjectID = require('mongodb').ObjectID;
const taskService = require('../services/taskService');

router.options('*', function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, POST, DELETE, GET');
  res.header('Access-Control-Allow-Headers', 'content-type, x-access-token');
  res.send();
});

// create task
router.put('/', function(req, res, next) {
  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');

  const document = req.body;
  document._id = new ObjectID();
  document.createdAt = new Date().getTime();
  document.comments = [];

  taskService.getLastOrder()
    .then(list => {
      document.order = list.length > 0 ? list[0].order + 1 : 0;
      taskService.createTask(document)
        .then(r => {
          res.status(201);
          res.send(document);
        })
        .catch(e => {
          res.status(500);
          res.send(e);
        });
    });
});

// update task
router.post('/', function(req, res, next) {
  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');

  const document = req.body;
  document._id = new ObjectID(document._id);

  taskService.updateTask(document)
    .then(r => {
      res.send(document);
    })
    .catch(e => {
      res.status(500);
      res.send(e);
    });
});

// update task order
router.post('/updateOrder', function(req, res, next) {
  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');

  const document = req.body;

  // taskService.updateOrder(new ObjectID(document._id), document.newIndex)
  taskService.updateOrder(document.oldIndex, document.newIndex, new ObjectID(document._id))
    .then(r => {
      res.send(document);
    })
    .catch(e => {
      res.status(500);
      res.send(e);
    });
});

// delete task
router.delete('/', function(req, res, next) {
  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');

  const document = req.body;
  document._id = new ObjectID(document._id);

  taskService.deleteTask(document._id)
    .then(r => {
      // return deleted task - FE needs to know which task was deleted
      res.send(document);
    })
    .catch(e => {
      res.status(500);
      res.send(e);
    });
});

module.exports = router;

module.exports = {
  db: {
    // url: `mongodb://${process.env.MONGO_USER || 'unknown'}:${process.env.MONGO_PWD || 'unknown'}@localhost:27017/svadba`,
    // url: `mongodb://${process.env.MONGO_USER || 'unknown'}:${process.env.MONGO_PWD || 'unknown'}@162.243.164.146:27017/svadba`,
    url: `mongodb://mibaro:j3iZ90@159.65.205.222:27017/mibaro`,

    tasksCollection: 'tasks',
    commentsCollection: 'comments',
    usersCollection: 'users'
  },
  auth: {
    secret: 'somethingawesomeaboutme' // TODO maybe dynamic generated secret will be better (so still another for each run BE app)
  }
};

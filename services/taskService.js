var mongoService = require('./mongoService');
const ObjectID = require('mongodb').ObjectID;

module.exports = {};

module.exports.getTasks = () => mongoService.getDb()
  .then(db => db.collection('tasks').find({}).sort({order: -1}).toArray());

module.exports.createTask = document => mongoService.getDb()
  .then(db => db.collection('tasks').insertOne(document));

module.exports.updateTask = document => mongoService.getDb()
  .then(db => db.collection('tasks').updateOne({_id: document._id}, {$set: document}));

module.exports.deleteTask = documentId => mongoService.getDb()
  .then(db =>
    db.collection('tasks').findOneAndDelete({_id: documentId})
      .then(r => db.collection('tasks')
        .find({order: {$gt: r.value.order}})
        .sort({order: 1})
        .forEach(
          doc => {db.collection('tasks').updateOne({_id: new ObjectID(doc._id)}, {$set: {order: doc.order - 1}})}
        )
      )
  );

module.exports.updateTaskOrder = (_id, newIndex) => mongoService.getDb()
  .then(db => {
    db.collection('tasks').updateOne({_id: _id}, {$set: {order: newIndex}});
  });

module.exports.getLastOrder = () => mongoService.getDb()
  .then(db => db.collection('tasks').find({}).sort({order: -1}).limit(1).toArray());

module.exports.updateOrder = (from, to, _id) => {
  return mongoService.getDb()
    .then(db => {
      if (from < to) {
          return db.collection('tasks')
          .find({order: {$gt: from, $lte: to}})
          .sort({order: 1})
          .forEach(
            doc => {db.collection('tasks').updateOne({_id: new ObjectID(doc._id)}, {$set: {order: doc.order - 1}})},
            () => {db.collection('tasks').updateOne({_id: _id}, {$set: {order: to}})}
          );
      } else if (to < from) {
          return db.collection('tasks')
          .find({order: {$gte: to, $lt: from}})
          .sort({order: 1})
          .forEach(
            doc => db.collection('tasks').updateOne({_id: new ObjectID(doc._id)}, {$set: {order: doc.order + 1}}),
            () => {db.collection('tasks').updateOne({_id: _id}, {$set: {order: to}})}
          );
      }
    })
};
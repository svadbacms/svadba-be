var mongoService = require('./mongoService');

module.exports = {};

module.exports.getUser = name => mongoService.getDb()
  .then(db => db.collection('users').findOne({name}));


module.exports.toggleNotifications = (name, sendNotifications) => mongoService.getDb()
  .then(db => db.collection('users').updateOne({name}, {$set: {sendNotifications}}));
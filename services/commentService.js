var mongoService = require('./mongoService');

module.exports = {};

module.exports.getComments = () => mongoService.getDb()
  .then(db =>
    new Promise((resolve, reject) => {
      db.collection('comments').aggregate([
        {$group: {_id: "$taskId", comments: {$push: "$$CURRENT"}}}
      ], function (e, result) {resolve(result)});
    }
  ));

module.exports.createComment = document => mongoService.getDb()
  .then(db => db.collection('comments').insertOne(document));

module.exports.updateComment = document => mongoService.getDb()
  .then(db => db.collection('comments').updateOne({_id: document._id}, {$set: {text: document.text, imageUrl: document.imageUrl}}));


module.exports.deleteTask = id => mongoService.getDb()
  .then(db => db.collection('comments').deleteOne({_id: id}));

